<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    <ul>
    @foreach($data as $key => $value)
            <li>
                <span>Name: {{ $value['name'] }}</span></br>
                <span>Price: {{ $value['price'] }}</span></br>
                <span>Rating: {{ $value['rating'] }}</span></br>
            </li>
    @endforeach
    </ul>
</body>
</html>
