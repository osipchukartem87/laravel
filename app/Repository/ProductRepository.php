<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use App\Services\ProductGenerator;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @param Product[] $products
     */
    protected $products;
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /**
     * @return Product[]
     */
    public function findAll(): array
    {
        return $this->products;
    }

}
