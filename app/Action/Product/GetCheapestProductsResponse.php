<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;
use App\Services\ProductGenerator;

class GetCheapestProductsResponse
{
    const AMMOUNT_OF_PRODUCTS = 3;

    protected $products;
    protected $productRepository;

    public function __construct()
    {
        $this->products = ProductGenerator::generate();
        $this->productRepository = new ProductRepository($this->products);
    }

    public function getProducts()
    {
        $products = $this->productRepository->findAll();
        foreach ($products as $product) {
            $prices[] = $product->getPrice();
        }

        array_multisort($prices, SORT_ASC, $products);

        return array_slice($products, 0, self::AMMOUNT_OF_PRODUCTS);

    }

}
