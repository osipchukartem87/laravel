<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetAllProductsAction
{
    public function execute(): GetAllProductsResponse
    {
        return new GetAllProductsResponse();
    }
}
