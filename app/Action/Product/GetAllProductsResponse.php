<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;
use App\Services\ProductGenerator;

class GetAllProductsResponse
{
    protected $products;
    protected $productRepository;

    public function __construct()
    {
        $this->products = ProductGenerator::generate();
        $this->productRepository = new ProductRepository($this->products);
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->productRepository->findAll();
    }
}
