<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    /**
     * @param Product[] $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        foreach ($products as $product) {
            $arrayProducts[] = self::present($product);
        }
        return $arrayProducts;
    }

    public static function present(Product $product): array
    {
            $productArray['id'] = $product->getId();
            $productArray['name'] = $product->getName();
            $productArray['price'] = $product->getPrice();
            $productArray['img'] = $product->getImageUrl();
            $productArray['rating'] = $product->getRating();

        return $productArray;
    }
}
