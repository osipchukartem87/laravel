<?php

namespace App\Http\Controllers;

use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Services\ProductGenerator;

class ProductController extends Controller
{
    public function getProducts()
    {
        return ProductArrayPresenter::presentCollection(ProductGenerator::generate());
    }

    public function getPopular()
    {
        $products = ProductArrayPresenter::presentCollection(ProductGenerator::generate());
        $products = $this->sortArrayByKey($products, 'rating');

        return array_pop($products);
    }

    public function getCheap(){
        $data = ProductArrayPresenter::presentCollection(ProductGenerator::generate());
        $data = $this->sortArrayByKey($data, 'price');

        return  view('cheap_products', ['data' => $data]);
    }

    private function sortArrayByKey($array, $field) {
        $sortedArr = array();
        foreach($array as $key => $val){
            $sortedArr[$key] = $val[$field];
        }

        array_multisort($sortedArr,$array);

        return $array;
    }
}
